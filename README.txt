====================================================
Natural Language Parsing
Genre and Sentiment Classification Demonstration
=====================================================
-----------------------------------------------------
Author: Francesco Liuzzi

credit to streamhacker.com for tutorials and info.
-----------------------------------------------------
DESCRIPTION

	This is or demonstration purposes, it was done for an intro
linguistics class.  See the containing pdf for more info.

------------------------------------------

USAGE:  
		ling_liuzzi -<app>  <filepath> 
ARGUMENTS:
       app:   sent, genre
       file:   filepath        <---- optional
